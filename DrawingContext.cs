﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameProject0
{
    public struct DrawingContext
    {
        public GameTime gameTime;
        public GraphicsDevice graphicsDevice;
        public SpriteBatch spriteBatch;
    }
}
