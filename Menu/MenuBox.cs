﻿using GameProject0.Shapes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace GameProject0.Menu
{
    public class MenuBox
    {
        public float BorderSize = 8f;
        public Color BackgroundColor = Color.White;
        public Color FontColor = Color.Black;
        public Color SelectedBackgroundColor = Color.Olive;

        private readonly Action _callBack;
        public SpriteFont SpriteFont;
        public string Text;

        public bool IsSelected = false;
        public Vector2 Position;

        public MenuBox(string text, SpriteFont spriteFont, Action callBack)
        {
            Text = text;
            SpriteFont = spriteFont;
            _callBack = callBack;
        }

        public void DoAction() { _callBack(); }

        public void Draw(DrawingContext context)
        {
            var measured = Measure();

            var rectTexture = RectangleTexture.Create(
                context.graphicsDevice,
                (int)measured.X,
                (int)measured.Y,
                IsSelected ? SelectedBackgroundColor : BackgroundColor
            );
            context.spriteBatch.Draw(rectTexture, Position, Color.White);

            context.spriteBatch.DrawString(SpriteFont, Text, Position + new Vector2(BorderSize), FontColor);
        }

        public Vector2 Measure()
        {
            return SpriteFont.MeasureString(Text) + new Vector2(2 * BorderSize);
        }
    }
}
