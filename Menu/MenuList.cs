﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace GameProject0.Menu
{
    public class MenuList
    {
        public List<MenuBox> Boxes { get; } = new List<MenuBox>();

        public float _spacing = 16f;
        public float Spacing
        {
            get => _spacing;
            set
            {
                _spacing = value;
                _updatePositions();
            }
        }

        private Vector2 _position = Vector2.Zero;
        public Vector2 Position
        {
            get => _position;
            set
            {
                _position = value;
                _updatePositions();
            }
        }

        private void _updatePositions()
        {
            float offset = 0;
            foreach (var box in Boxes)
            {
                var measured = box.Measure();
                box.Position = Position + new Vector2(0, offset + measured.Y);
                offset += measured.Y + Spacing;
            }
        }

        public MenuBox AddBox(MenuBox box)
        {
            Boxes.Add(box);
            return box;
        }

        public Vector2 Measure()
        {
            Vector2 dimension = new Vector2(0, Spacing * Math.Max(0, Boxes.Count - 1));
            foreach (var box in Boxes)
            {
                var measuredBox = box.Measure();
                dimension.X = Math.Max(measuredBox.X, dimension.X);
                dimension.Y += measuredBox.Y;
            }
            return dimension;
        }

        public void Draw(DrawingContext context)
        {
            foreach (MenuBox box in Boxes)
            {
                box.Draw(context);
            }
        }
    }
}
