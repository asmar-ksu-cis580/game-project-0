﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace GameProject0.Menu
{
    public class MainMenu
    {
        public MenuList menuList = new();

        private bool _shouldExit = false;
        public bool ShouldExit { get => _shouldExit; }

        public string FooterText = "Press Enter to select.";

        public Vector2 Position
        {
            get => menuList.Position;
            set => menuList.Position = value;
        }
        public SpriteFont SpriteFont { get; }

        public MainMenu(SpriteFont spriteFont)
        {
            (string, Action)[] items = {
                ("New game", () => {}),
                ("Options", () => {}),
                ("Exit the game", () => { _shouldExit = true; }),
            };
            foreach (var (label, action) in items)
            {
                menuList.AddBox(new MenuBox(label, spriteFont, action));
            }
            // FIXME
            menuList.Boxes[2].IsSelected = true;
            SpriteFont = spriteFont;
        }

        public Vector2 Measure()
        {
            var menuListMeasured = menuList.Measure();
            var footerTextMeasured = SpriteFont.MeasureString(FooterText);
            return new Vector2(
                Math.Max(menuListMeasured.X, footerTextMeasured.X),
                menuListMeasured.Y + footerTextMeasured.Y
            );
        }

        public void Draw(DrawingContext context)
        {
            menuList.Draw(context);
            var yCoord = menuList.Measure().Y;
            context.spriteBatch.DrawString(SpriteFont, FooterText, new Vector2(Position.X, yCoord), Color.White);
        }
    }
}
