﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace GameProject0.Shapes
{
    public class RectangleTexture
    {
        public static Texture2D Create(GraphicsDevice graphicsDevice, int width, int height, Color color)
        {
            var texture = new Texture2D(graphicsDevice, width, height);
            var totalRectArea = width * height;
            Color[] data = new Color[totalRectArea];
            Array.Fill(data, color);
            texture.SetData(data);
            return texture;
        }
    }
}
