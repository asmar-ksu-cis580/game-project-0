﻿using System;
using GameProject0;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace InputExample
{
    public class SandTile
    {
        Random random = new Random();

        Game game;

        Texture2D texture;

        public Vector2 Position;

        public SandTile(Game game, Vector2 position)
        {
            this.game = game;
            Position = position;
        }

        public void LoadContent()
        {
            texture = game.Content.Load<Texture2D>("sand_32x32");
        }

        public void Update(GameTime gameTime)
        {
            Position.Y += (float)gameTime.ElapsedGameTime.TotalSeconds * 100;

            var viewport = game.GraphicsDevice.Viewport;
            if (Position.Y > viewport.Height)
            {
                Position = new Vector2(
                    (float)random.NextDouble() * viewport.Width, 
                    -texture.Height
                );
            }
        }

        public void Draw(DrawingContext context)
        {
            if (texture is null) throw new InvalidOperationException("Texture must be loaded to render");
            context.spriteBatch.Draw(texture, Position, Color.White);
        }
    }
}
