﻿using System;
using GameProject0;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace InputExample
{
    public class BrickWall
    {
        Game game;

        Texture2D texture;

        public Vector2 Position;

        public BrickWall(Game game, Vector2 position)
        {
            this.game = game;
            Position = position;
        }

        public void LoadContent()
        {
            texture = game.Content.Load<Texture2D>("brick-wall_128x32");
        }

        public void Draw(DrawingContext context)
        {
            if (texture is null) throw new InvalidOperationException("Texture must be loaded to render");
            context.spriteBatch.Draw(texture, Position, Color.White);
        }
    }
}
