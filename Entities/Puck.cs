﻿using System;
using GameProject0;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace InputExample
{
    public class Puck
    {
        Random random = new Random();

        Game game;

        Texture2D texture;

        public Vector2 Direction;
        public Vector2 Position;

        public float Rotation = 0f;

        public Puck(Game game, Vector2 direction, Vector2 position)
        {
            this.game = game;
            Direction = direction;
            Position = position;
        }

        public void LoadContent()
        {
            texture = game.Content.Load<Texture2D>("puck");
        }

        public void Update(GameTime gameTime)
        {
            var axisDelta = (float)gameTime.ElapsedGameTime.TotalSeconds * 100;
            Position += axisDelta * Direction;

            // TODO Why <= texture.Height / 2 after rotating?
            var viewport = game.GraphicsDevice.Viewport;
            if (Position.Y >= viewport.Height - texture.Height / 2 || Position.Y <= texture.Height / 2)
            {
                Direction.Y *= -1;
            }
            if (Position.X >= viewport.Width - texture.Width / 2 || Position.X <= texture.Width / 2)
            {
                Direction.X *= -1;
            }

            Rotation += (float)gameTime.ElapsedGameTime.TotalSeconds;
        }

        public void Draw(DrawingContext context)
        {
            if (texture is null) throw new InvalidOperationException("Texture must be loaded to render");
            var origin = new Vector2(texture.Width / 2, texture.Height / 2);
            context.spriteBatch.Draw(texture, Position, null, Color.White, Rotation, origin, 1f, SpriteEffects.None, 0);
        }
    }
}
