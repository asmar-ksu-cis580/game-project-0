﻿using GameProject0.Menu;
using InputExample;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace GameProject0
{
    public class GameProject0 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private SpriteFont _spriteFont;

        DrawingContext drawingContext;

        private MainMenu _mainMenu;

        private List<SandTile> sandTiles = new List<SandTile>();
        private List<BrickWall> brickWalls = new List<BrickWall>();
        private Puck puck;

        public GameProject0()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            var random = new Random();

            var viewport = _graphics.GraphicsDevice.Viewport;
            for (int i = 0; i < 5; ++i)
            {
                var position = new Vector2(
                    (float)random.NextDouble() * viewport.Width,
                    (float)random.NextDouble() * viewport.Height
                );
                sandTiles.Add(new SandTile(this, position));
            }

            for (int i = 0; i < 2; ++i)
            {
                var position = new Vector2(
                    (float)random.NextDouble() * viewport.Width,
                    (float)random.NextDouble() * viewport.Height
                );
                brickWalls.Add(new BrickWall(this, position));
            }

            puck = new Puck(
                this,
                new Vector2((float)random.NextDouble(), (float)random.NextDouble()),
                new Vector2(
                    (float)random.NextDouble() * viewport.Width,
                    (float)random.NextDouble() * viewport.Height
                )
            );

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            _spriteFont = Content.Load<SpriteFont>("RubikDirt");

            drawingContext = new DrawingContext();
            drawingContext.graphicsDevice = _graphics.GraphicsDevice;
            drawingContext.spriteBatch = _spriteBatch;

            foreach (var sandTile in sandTiles)
            {
                sandTile.LoadContent();
            }
            foreach (var brickWall in brickWalls)
            {
                brickWall.LoadContent();
            }
            puck.LoadContent();

            _mainMenu = new MainMenu(_spriteFont);
            _mainMenu.Position = Vector2.Zero;
            var viewport = _graphics.GraphicsDevice.Viewport;
            var measured = _mainMenu.Measure();
            _mainMenu.Position = new Vector2(viewport.Width / 2, viewport.Height / 2) - measured / 2;
        }

        protected override void Update(GameTime gameTime)
        {
            var keyboardState = Keyboard.GetState();
            if (keyboardState.IsKeyDown(Keys.Enter))
                Exit();

            foreach (var sandTile in sandTiles)
            {
                sandTile.Update(gameTime);
            }

            puck.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            drawingContext.gameTime = gameTime;

            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();

            foreach (var sandTile in sandTiles)
            {
                sandTile.Draw(drawingContext);
            }

            foreach (var brickWall in brickWalls)
            {
                brickWall.Draw(drawingContext);
            }

            puck.Draw(drawingContext);

            _mainMenu.Draw(drawingContext);

            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}